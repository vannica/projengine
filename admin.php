<?php   
 
class Admin_Controller extends Base_Controller {

    // Our first stuff
    public $restful = true;
    public $view = 'admin';
    public $path_ctrl = 'admin/';
    public function __construct(){
        parent::__construct();
        // $this->filter('before', 'auth');
     
    }

    /*
    * Esegue il check sull'accesso alla risorsa per l'utente loggato
    * @param $model , $act (create,read,update,delete)
    */
    public function check_access($model,$act) {

    }

    public function get_index() {
        return View::make('admin.dashboard',array('user' => Sentry::user()->get()));
    }


    public function get_list($model = null ,$parent=null,$parent_id = null) {
      
       $obj = IoC::resolve($model);
       $data['obj'] = $obj;
       $data['conf'] = $obj->conf(); 
      
       if($parent && $parent_id ) 
       {    
            $data['parent'] = new stdClass();

            if(class_exists($parent)) {
              $obj_parent = IoC::resolve($parent); 

              //// controllo che sia di proprietà 
              if( !$obj_parent->isOwn($parent_id) ) return Redirect::to('admin/');
                         
              $data['parent']->obj = $obj_parent->find($parent_id);              
            }            

            $data['parent']->model = $parent;
            $data['parent']->id = $parent_id;

        }else if( $obj->getOnlyWParent() ) return Response::error('404');

        if (Input::get('db')) {   return  json_encode($obj->serve($obj,$data)) ;    } /// per debug 

        if (Request::ajax() && !Input::get('html') ) 
        {      
            // dt per datables la seconda serve per i combo box                             
            return (Input::get('dt')) ? json_encode($obj->serve($obj,$data)) : json_encode($obj->getJson($obj,$data)); 
        }       
        else 
        {   
            //// Evento all'apertura della pagina/model
            Event::fire('admin.getlist', array($model, $data));
            return (View::exists('admin.'.$model)) ?  View::make('admin.'.$model,$data) : View::make('admin.list',$data);                          
        } 

    }

    public function get_edit($model = null ,$id = null, $parent = null , $id_parent = null)
    {       

            $view =  (Request::ajax()) ? "editajx" : "edit";
             
            $obj_model = IoC::resolve($model);    



            $data["conf"] = $obj_model->conf();
        
            if( $parent && $id_parent )
            {
                $obj_parent = IoC::resolve($parent); 
                //// controllo che sia di proprietà 
                if( !$obj_parent->isOwn($id_parent) ) return Redirect::to('admin/');

                $data['parent'] = $obj_parent->find($id_parent);
                $data['parent']->model = $parent; 
                $data['parent']->id = $id_parent;
            }

            if($id) 
            {           
                $obj = $obj_model->find($id);                
                //// controllo che sia di proprietà 
                if( !$obj_model->isOwn($id) ) return Redirect::to('admin/');

                if($obj) 
                { 
                    $data["data"] = $obj;                      
                    $data["copy"] = (Input::has("copy") && Input::get("copy"))  ? true : false ;       
                }               
            }

             ///User EXIT edit_open
            if( method_exists($obj_model, "edit_open" )) $obj_model = $obj_model->edit_open($model,$id,$parent,$id_parent);

            return (View::exists('admin.editcustom.edit-'.$model)) ?  View::make('admin.editcustom.edit-'.$model,$data) : View::make($this->view.'.'.$view,$data); 
    }

   
    public function get_massedit($model = null ,$id = null)
    {       

            $view =  (Request::ajax()) ? "masseditajx" : "massedit";
             
            $obj_model = IoC::resolve($model);
            $data["conf"] = $obj_model->conf();
            
            return (View::exists('admin.editcustom.massedit-'.$model)) ?  View::make('admin.editcustom.massedit-'.$model,$data) : View::make($this->view.'.'.$view,$data); 
    }

    public function get_massdelete($model = null ,$id = null)
    {
            $view =  (Request::ajax()) ? "massdeleteajx" : "massdelete";
             
            $obj_model = IoC::resolve($model);
            $data["conf"] = $obj_model->conf();
            
            return (View::exists('admin.editcustom.massdelete-'.$model)) ?  View::make('admin.editcustom.massdelete-'.$model,$data) : View::make($this->view.'.'.$view,$data); 
    }



    public function post_edit($model = null ,$id = null)
    {   
         $saved_ids = array();
         //$lingue = Property::get_allLanguage();
         $obj_model = IoC::resolve($model);
         $data["conf"] = $obj_model->conf();
         $input_id = Input::get('id');

         $id = (Input::has('id') && !empty($input_id[0]) && $input_id[0]) ? Input::get('id') : null;
         $rules = $data["conf"]->val_array($id);
          
        $validation = Validator::make(Input::all(), $rules);
        if ($validation->fails())
        {
            
            $msg = array("status" => 0,"error" =>1 , "msg" => InappMsg::add_forjson('error',$validation->errors->all()));
            //// svuotare InappMsg se la chiamata arriva in ajax alrimenti vengono propagati alle altre pagine 
            return (Request::ajax()) ?  Response::json($msg)  : Redirect::to('admin/edit/'.$model."/".$id)->with_input();

        }else{

            for($i = 0; (isset($id)) ? $i < count($id) : $i < 1 ; $i++ )
            {
            
                $campi_screen = Input::except("id");

                ////// modifica combo-source-multi
                if($multi = $obj_model->conf()->getComboMultiFields() )
                {
                    foreach ($multi as $key => $value) {
                       if(array_key_exists($value, $campi_screen))
                        $campi_screen[$value] = implode(Config::get('custom.separator'), $campi_screen[$value]);
                    else $campi_screen[$value] = ""; 
                    }
                }
                ///////////////////////

                if($id) { $obj_model = $obj_model->find($id[$i]);  }
                
                $obj_model->fill($campi_screen);
                

                ///User EXIT form_post
                // controlla se esiste un metodo all'interno dell oggetto con nome post e nel caso lo chiama // restituisce l'oggetto stesso;
                if( method_exists($obj_model, "form_post" )) $obj_model = $obj_model->form_post();

                
                if($obj_model->save()) 
                {   

                     // $saved_ids[] = $id;
                    // $str_ids = (!is_array($id)) ? $id : implode(" - ", $saved_ids );
                    if($id) InappMsg::add('success','ID: '.$id[$i].' - Oggetto/i modificato/i correttamente');
                    else InappMsg::add('success','Nuovo oggetto aggiunto correttamente');
                 //   Langdescription::saveDescrFields($campi_screen,$obj_model);   
                }
                else 
                {
                    InappMsg::add('warning','Errore durante il savataggio');
                    return Redirect::to('admin/'.$view.'/'.$model."/".$id)->with_input();
                }

            }               
            
            $msg = array("status" => 1,"error" =>0 , "msg" => InappMsg::get_html());
            
            return (Request::ajax()) ? Response::json($msg)  : Redirect::to('admin/'.$model);
        }
    }


    public function get_delete($model = null ,$id = null)
    {    

        if(isset($id) && isset($model)) 
        {  

            $obj_model = IoC::resolve($model);
            
            //// controllo che sia di proprietà 
            if( !$obj_model->isOwn($id) ) return Redirect::to('admin/');


            $data["conf"] = $obj_model->conf();
            $obj = $obj_model->find($id);
            $data["data"] = $obj;
            
            if(!Request::ajax()) 
            { // da controllare se viene usata in qualche chiamata 
                 // $obj->delete()  ;
                  InappMsg::add('success','Oggetto ( ID: '.$id.' ) eliminato correttamente');
              //  else InappMsg::add('error','Errore durante la cancellazione');
                return Redirect::to('admin/'.$model);
            }
            else 
            {
                return View::make($this->view.'.deleteajx',$data) ;
            }

        }
    }

    public function post_delete($model = null ,$id = null)
    {   

        if(isset($model) && Request::ajax()) 
        {
            $obj_model = IoC::resolve($model);
            $data["conf"] = $obj_model->conf();
            
            if(isset($id)) 
            {          
                $obj = $obj_model->find($id);
                $obj->delete(); 
            }

            if(!isset($id) &&  (Input::has('id') ) )
            {    
                $id_arr = Input::get("id");
                if( is_array($id_arr) && $id_arr[0] )
                {
                    foreach ($id_arr  as $key => $value) 
                    {
                          // $obj_model->where_in( "id", $id_arr )->delete();
                          $obj_model->find( $value )->delete();
                    }   
                    
                }
            }    
        $id = ($id) ? $id : implode(" - ", $id_arr );
        InappMsg::add('success','Oggetto/i ( ID: '.$id.' ) eliminato correttamente');
        
       // else InappMsg::add('error','Errore durante la cancellazione');

        $msg = array("status" => 1,"error" =>0 , "msg" => InappMsg::get_html());
        
        return Response::json($msg);

        }
    }

  
 

}
