<?php

class AdmPermessi {
        
    private $lista_permessi;
    
    public function __construct() {
    
    }
    
    public static function GetLista() {   
        //Recupero la lista di rule
        $lista_permessi = Rule::all();        
        return $lista_permessi;
        }

    ////// Restituisce il gruppo dell'utente loggato
    /// da upgradare con ritorno array gruppi in caso di utente appartenente a più gruppi
    public static function getLoggedGrpName()
    {
      $usgroupdata = Sentry::user()->groups();
      $usgroup = ($usgroupdata) ? $usgroupdata[0]['name'] : 'VND' ;
      return $usgroup;
    }

    ///// Restituisce un array con chiave = gruppo => valore = array(CRUD permessi)
    //// parametri : stringa permessi proveniente da un campo (es: VND:0010,AGT:1110 )
    public static function array_permission($permessi)
    { 
      $result = array();
      //Estraggo permessi del campo come 'Gruppo' => CRUD(1111)
      $xgroup = explode(',',$permessi);   
      foreach($xgroup as $gruppo)
      {
        $gruppo_p = explode(':',$gruppo);
        $crud = array();
        
        list($crud["C"],$crud["R"],$crud["U"],$crud["D"]) = str_split($gruppo_p[1]);

        $result[$gruppo_p[0]] = $crud;
          
        }
      return $result;
    }

    //// controlla i permessi per una action passando i permessi dell'action
    /// viene usata nella serve->create action row
    public static function check_perm_action($permessi) {

      if($permessi)
        {
          $usgroupdata = Sentry::user()->groups();
          $usgroup = $usgroupdata[0]['name'];

          /// VND:0000,AGT:1100
          $permcampo = explode(',',$permessi);   
          
          foreach($permcampo as $gruppo){
            $group_p = explode(':',$gruppo);
            if($group_p[0] == $usgroup){
                                          $permgroup = $group_p[1];
                                          break;    
             }
          }
          if(!isset($permgroup)) return true;
          if(isset($permgroup) && $permgroup != "0000") return true;
          else return false;
        }
      else return true;

    }

    // check permesso/azione - azione es "C", "CR", "CU"
    public static function checkPerm($act = "" , $permessi = null)
    {
      $passed = 1;
      if($permessi)
      {
          $user_group =  self::getLoggedGrpName();
          $perm_obj = self::array_permission($permessi);
          if(array_key_exists($user_group, $perm_obj))
          {
            foreach (str_split($act) as $key => $value) {
             
             if( !array_get($perm_obj, $user_group.'.'.$value) ) $passed = 0;
            
            }
          }
      }
      return $passed;
    }


    
    public static function get_viewtp_field($permessi = null,$act){
      //Senza permessi, default tutti
      //gruppo utente

      // $usgroupdata = Sentry::user()->groups();
      // if($usgroupdata) $usgroup = $usgroupdata[0]['name'];
      // else $usgroup = "AGT";
      $usgroup = self::getLoggedGrpName();

      if(!$permessi) return 'E';
        
      //Estraggo permessi del campo come 'Gruppo' => CRUD(1111)
      $permcampo = explode(',',$permessi);   
      foreach($permcampo as $gruppo){
        $group_p = explode(':',$gruppo);
        if($group_p[0] == $usgroup){
            $permgroup = str_split($group_p[1]);
            break;    
         }
      }


      if(!isset($permgroup)) return 'E';
      //$permgroup[0] = C
      //$permgroup[1] = R
      //$permgroup[2] = U
      //$permgroup[3] = D
      if($act == 'U') {
           //Modifica
          if($permgroup[2]){
            //Permesso alla modifica
            return 'E';
            }
          elseif($permgroup[1]){
            //Permesso alla visualizzazione - Label
            return 'V';
            }
          else {
            //Nessun permesso - hidden
            return 'H';
          }
      }
      else{
        //Creazione
        if($permgroup[0]){
            //Permesso alla creazione
            return 'E';
            }
        else {
            //Nessun permesso - hidden
            return 'H';
          }  
      }        
    }
    
  /**
   *  Crea la lista delle rule ordinate per model da checkckare
   *  
   *  @param  $data
   *  @return stringa html
   */
    public static function rule_selection($data)
    {
       $lista = AdmPermessi::GetLista(); 
       $found = array();
       $element["html"] = '<div class="control-group">';
      foreach( $lista as $rule ){

          $id_ckperm = $rule->rule;
          $model_rule = explode("_",$rule->rule);

          if(!in_array($model_rule[0], $found)) 
          {   
            $found[] = $model_rule[0];
            $element["html"] .= '</div><hr />';
             $element["html"] .= '<span class="span2"> <strong>'.strtoupper($model_rule[0]).'</strong></span>';
            $element["html"] .= '<div class="control-group">';
           

          }
          //$element["html"] = '<div class="control-group">';
          $label = (count($model_rule) == 2) ? $model_rule[1][0] : $rule->rule;
          $element["html"] .= "<span> ".strtoupper($label)." ";                                 
          $element["html"] .=  Form::checkbox($id_ckperm, 1, Input::old($id_ckperm,Makeform2::setOrNullCkPerm($data,"permissions",$id_ckperm)))."</span> ";
                
        }
        $element["html"] .= "<br /><hr />";
        return $element;
    }

  
  
}

?>