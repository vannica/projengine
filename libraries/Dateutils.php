<?php

class Dateutils 
{	  
    //public static $menu ;

    ///nome , model , menu_parent
 
    public function __construct()
	{   
	   
	}

   public static function datediff($tipo, $partenza, $fine)
    {
        switch ($tipo)
        {
            case "A" : $tipo = 365;
            break;
            case "M" : $tipo = (365 / 12);
            break;
            case "S" : $tipo = (365 / 52);
            break;
            case "G" : $tipo = 1;
            break;
        }
        $arr_partenza = explode("/", $partenza);
        $partenza_gg = $arr_partenza[0];
        $partenza_mm = $arr_partenza[1];
        $partenza_aa = $arr_partenza[2];
        $arr_fine = explode("/", $fine);
        $fine_gg = $arr_fine[0];
        $fine_mm = $arr_fine[1];
        $fine_aa = $arr_fine[2];
        $date_diff = mktime(12, 0, 0, $fine_mm, $fine_gg, $fine_aa) - mktime(12, 0, 0, $partenza_mm, $partenza_gg, $partenza_aa);
        $date_diff  = floor(($date_diff / 60 / 60 / 24) / $tipo);
        return $date_diff;
    }
  
public static function datediff_USA($tipo, $partenza, $fine)
    {
        switch ($tipo)
        {
            case "A" : $tipo = 365;
            break;
            case "M" : $tipo = (365 / 12);
            break;
            case "S" : $tipo = (365 / 52);
            break;
            case "G" : $tipo = 1;
            break;
        }
        $arr_partenza = explode("-", $partenza);
        $partenza_gg = $arr_partenza[2];
        $partenza_mm = $arr_partenza[1];
        $partenza_aa = $arr_partenza[0];
        $arr_fine = explode("-", $fine);
        $fine_gg = $arr_fine[2];
        $fine_mm = $arr_fine[1];
        $fine_aa = $arr_fine[0];
        $date_diff = mktime(12, 0, 0, $fine_mm, $fine_gg, (int) $fine_aa) - mktime(12, 0, 0, $partenza_mm, $partenza_gg, (int) $partenza_aa);
        $date_diff  = floor(($date_diff / 60 / 60 / 24) / $tipo);
        return $date_diff;
    }


  public static function toita($mysql_datetime,$withtime = null) 
  {
     if(is_object($mysql_datetime)) return $mysql_datetime;

     $datetime = strtotime( $mysql_datetime);
     if(!$withtime) $formatted = strftime("%d/%m/%Y ",$datetime);
     else $formatted = strftime("%d/%m/%Y %T", $datetime);
     return $formatted;
    
   }

  public static function tomysql($mysql_datetime) 
  {
      if(is_object($mysql_datetime)) return $mysql_datetime;
            
      $datetime = strtotime(str_replace('/', '-', $mysql_datetime));
      $formatted = date("Y-m-d", $datetime);
      return $formatted;
     
  }

  /// aggiunge X giorni alla data fornita 
  public static function addXdays($data, $days,$input = "IT" ,$output = "IT")
  {     
      $data = ($input == "IT") ? self::tomysql($data) : $data;

      $added =  strtotime( $data .' +'.$days.' days');

      return (strtoupper($output) == "IT") ? strftime("%d/%m/%Y ",$added) : date("Y-m-d" , $added); 
  }





}