<?php 
class InappMsg {

     public static $msgss = array();

     /**
      * Add a message to the message array (adds to the user's session)
      * @param string  $type    You can have several types of messages, these are class names for Bootstrap's messaging classes, usually, info, error, success, warning
      * @param string $message  The message you want to add to the list
      */
     public static function add($type = 'info',$message = false){
      if(!$message) return false;
      if(is_array($message)){
        foreach($message as $msg){
          static::$msgss[$type][] = $msg;
        }
      }else{
        static::$msgss[$type][] = $message;
      }
      Session::flash('messages', static::$msgss);
     }

        /**
      * Crea un array associativo con campo => messaggio, per le chiamate in ajax da validare js
      * @param string  $type   
      * @param string $message  
      */
     public static function add_forjson($type = 'info',$message = false){
      $validation_msg = array();
      if(!$message) return false;
      if(is_array($message)){
        foreach($message as $msg){
          /// formato mesg:  nome_campo : testo_msg
          $msg_arr = explode(":", $msg);
          $validation_msg[] = array("id" => str_replace(" ", "_", $msg_arr[0]), "text" => $msg_arr[1]);
        }
      }else{
        $msg_arr = explode(":", $message);
        $validation_msg[] = array("id" => str_replace(" ", "_", $msg_arr[0]), "text" => $msg_arr[1]);
      }
      return $validation_msg;
     }

     /**
      * Pull back those messages from the session
      * @return array
      */
     public static function get(){
      return Session::get('messages');
     }
    
     /**
      * Gets all the messages from the session and formats them accordingly for Twitter bootstrap.
      * @return string
      */
     public static function get_html(){
      $messages = Session::get('messages');
      $output = false;
      if($messages){
        foreach($messages as $type=>$msgs){
          $output .= '<div class="alert alert-'.$type.'"><a class="close" data-dismiss="alert">×</a>';
          foreach($msgs as $msg){
            $output .= ''.$msg.'<br />';
          }
          $output .= '</div>';
        }
      }
      return $output;
     }

        /**
      * Gets all the messages from the session and formats them accordingly for Twitter bootstrap.
      * @return string
      */
     public static function clean(){
     
     return Session::forget('messages');
   
     }
}