<?php

class Menu 
{	  
    public static $menu ;

    ///nome , model , menu_parent
 
    public function __construct()
	{
	   
	   
	}

  public static function isActive($model = null)
  {
    return (URI::segment(2) == $model) ? "active" : "";
  }

	public static function getMenu()
	{

	 $contents = File::get(path('app')."models/conf/menu.json");   
  	 $menu = json_decode($contents); 
  	 $html = ' <li class="'.self::isActive().' dashboard_link"><a href="'.URL::to_action('admin').'">Dashboard</a></li>';
  	 $found = array();
     foreach ($menu as $value) {
     	
     	if (!Sentry::user()->has_access($value->model.'_read') || !Sentry::user()->has_access($value->model.'_view')) continue ;
     	if($value->menu_parent && !in_array($value->menu_parent, $found)) 
     	{   
     		$found[] = $value->menu_parent;
     		$html .= self::createParent($menu,$value->menu_parent); 
     	}
     	if(!$value->menu_parent) $html .= '<li class="'.self::isActive($value->model).'"><a href="'.URL::to('admin/'.$value->model).'">'.$value->nome.'</a></li>' ;

      }

     return $html;

	}
	    
    public static function createParent($menu,$parent)
    {  
       
       $dropdown = ""; $actclass= null;

       foreach ($menu as $value) 
       {
         	if (!Sentry::user()->has_access($value->model.'_read') || !Sentry::user()->has_access($value->model.'_view')) continue ;

          
         	
          if($value->menu_parent == $parent) 
          {
              if(!$actclass ) $actclass = self::isActive($value->model)  ;
              $dropdown .= '<li class="'.self::isActive($value->model).'"><a href="'.URL::to('admin/'.$value->model).'">'.$value->nome.'</a></li>' ;
          }
       }

      $dropdown .= ' </ul></li>';

      $dropdown_head = '<li class="dropdown '.$actclass.'">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$parent.' <b class="caret"></b></a>
                            <ul class="dropdown-menu">' ;


      return $dropdown_head.$dropdown;
    }





}