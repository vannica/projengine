<?php

class Utils 
{	  
    //public static $menu ;

    ///nome , model , menu_parent
 
    public function __construct()
	{   
	   
	}

  public static function form_attributes($specify_id = null )
  {
    return array('class'=>'form-horizontal form_ajx','id'=> $specify_id.'_form_ajx');
  }

   

  ///// restituisce una tabella html con i dati di un array ( bidimensionale )
  public static function array_totable($array  )
  {
    $html = '<table>';
    foreach ($array as $key => $value) {
      $html .='<tr>';
      foreach ($value as $k2 => $v2) { $html .= '<td>'.$v2.'</td>';  }
      $html .='</tr>';
    }
    $html .= '</table>';
    return $html;
  }


 /**
   * Rimuove http:// da un url generato con URL::
   * utilizzato negli hook in cui passiamo gli url base o admin
   * @param  $url string
   * @return $url updated
   */
   public static function remHttpUrl($url)
   {
        return str_replace("http://", "" , $url);
   }

  /// restituisce un popover html
  // param -> un array con "hooks" => "valore" e altri parametri html 
  public static function popover($hooks = array() , $place = "top", $icon = "icon-info-sign" , $extraclass= null )
  {

    $popover = '<a href="#" class="btn btn-mini popinfo '.$extraclass.'" data-toggle="popover" data-placement="'.$place.'" 
                        data-content="##POPOVER_CONTENT##"
                        title="##POPOVER_TITLE##" 
                        data-title="##POPOVER_TITLE##"><i class="'.$icon.' icon"></i></a>';

    return self::hooking($hooks,$popover);
  }

  //// sostituisce gli hooks in un contenuto 
  // param -> un array con chiave = hook => valore = dato da sostituire
  public static function hooking($hok_and_data = array() , $container )
  {
    $hooked = str_replace(array_keys($hok_and_data), $hok_and_data , $container);
    return $hooked;
  } 

   /**
   * Sostituisce gli hook presenti in contents ( array di contenuti )
   * con i valori degli oggetti es ##CUSOMER_NAME##  presente in un contents
   * viene sostituito da model->field  con model preso dagli objects
   *
   * @param  $objects ( oggetti necessari al riempimento ) , $contents array di contenuti con hooks
   * @return $content con gli hook sostituiti
   */

  public static function hookingObjects($objects = array() , $content = array() )
  {
    $result = preg_replace_callback( 
                      '/##([A-Z]*)_([A-Z]*)##/', 
                      function($match) use ($objects) 
                      { 
                        ////devo effettuare il replace in match[0] ho ##CUSTOMER_NAME##
                        // in match[1] avrò il model e in match[2] il campo
                                            /// LINK
                                            if($match[1] == "LINK")
                                            {
                                                if($match[2] == "BASE") return self::remHttpUrl(URL::base());
                                                if($match[2] == "ADMIN") return self::remHttpUrl(URL::to('admin'));
                                            }
                                            //// MODEL
                                            if(class_exists($match[1]))
                                            {
                                                $model = strtolower($match[1]); 
                                                $field = strtolower($match[2]);
                                                return (array_key_exists($model, $objects)) ? $objects[$model]->{$field} : ""; 
                                            }       

                      }, 
                      
                      $content
                      ); 
  
      return $result;
  }
















  /// STRIPPING mail e numeri telefono
  // ritorna array con data => contenuto , count => numero occorrenze sostituitr 
  public static function rem_content($where,$what = null,$replacement = "rimosso")
  {
    ///RULES
    $pattern = array();
    $pattern["[mail]"] = "/[^@\s]*@[^@\s]*\.[^@\s]*/";
    $pattern["[phone]"] = "/\+?[0-9][0-9()-\s+]{4,20}[0-9]/";
    $pattern["[isp]"] = "/virgilio.it|gmail.com|hotmail.it|hotmail.com|libero.it|email.it|yahoo.it|yahoo.com/";

    if($what)
    {
      foreach ($what as $key => $value) { $toremove[$value] = $pattern[$value];    }  
    }
    else  {  $toremove = $pattern;  }

    $repl_arr = array_keys($toremove);
    $count = 0;
    $data = preg_replace($toremove, $repl_arr, $where,-1,$count);

    return array("data" => $data , "count" =>  $count);
  }


}