<?php
   
Class Confmodel 
{

  public  $fields;
  public  $name_page;
  public  $model;
  public  $actions;
  
  function __construct($name_p,$mod,$id = null,$fields = null,$actions = null){ 
    $this->fields    =  $fields;
    $this->name_page =  $name_p;
    $this->model     =  $mod;
    $this->id        =  $id;
    $this->actions   =  $actions;
    //$this->makeFields();
    //$this->makeActions();
  } 
  



  public function appendField($field){
    $this->fields[] = $field;                      
  }

  public function makeFields() {

 
   $contents = File::get(path('app')."models/conf/".$this->model.".json");   
   $obj = json_decode($contents);
   $this->fields = $obj->fields;

   //return $obj->fields;
   //print_r($this->fields);

   // $this->fields = DB::table('fields')
   //  ->left_join('modelconfigs', 'fields.id', '=', 'modelconfigs.field_id')
   //  ->where('fields.resource_id','=',$this->id)
   //  ->order_by('order','DESC')
   //  ->get();

  }

  public function makeActions() {  

   $contents = File::get(path('app')."models/conf/".$this->model.".json");
   $obj = json_decode($contents);
   $this->fields = $obj->actions;

   // $this->actions = DB::table('actions')
   //  ->where('actions.resource_id','=',$this->id)
   //  ->order_by('order','DESC')
   //  ->get();

  }


   /**
   * Ritorna l'array delle action con tipo = link e i permessi validi
   * @param  
   * @return array
   */

  public function getLinkActions()
  {

    $link_actions = array();
    foreach( $this->actions as $i => $act )
    {
     if($act->tipo == "link" && !$act->visible && AdmPermessi::checkPerm("V", $act->permessi ) ) $link_actions[] = $act;
    }

    return $link_actions;
  }



   /**
   * Ritorna l'array dei field per la tabella
   * @param  
   * @return array con key = nome colonna , valore = fx_view impostato nel conf
   */

  function getVisibleColumns() 
  { 
    $columns = array();
    foreach ($this->fields as  $f) {
         
          if($f->head_table) $columns[$f->nome_campo] = explode(',',$f->fx_view); //in 0 il model , in 1 il campo 
    }
    return $columns;
  }

    /**
   * Ritorna l'array dei field del db
   * @param  
   * @return array con key = nome colonna , valore = fx_view impostato nel conf
   */

  function getDbColumns() 
  { 
    $columns = array();
    $columns["id"] = ""; 
  
    foreach ($this->fields as  $f) {
         
          if($f->head_table && $f->tipo != "solo_view" ) $columns[$f->nome_campo] = explode(',',$f->fx_view); //in 0 il model , in 1 il campo 
    }
    return $columns;
  }

   /**
   * Ritorna true se sono presenti campi abilitati alla modifica massiva
   * @param  
   * @return boolean
   */

  function getMassiveIndicator() 
  { 
    $columns = array();
    foreach ($this->fields as  $f) {
          //if($f->head_table) $columns[] = $f->nome;
         
          if($f->head_table && $f->massive) return true;
    }
    return false;
  }  

  public function getComboMultiFields()
  {
    $fields = array();
    foreach ($this->fields as  $f) {         
          if($f->tipo == 'combo-source-multi') $fields[] = $f->nome_campo;
    }
    return $fields;
  }


  //// MOD INLINE EDITING 02-06-2013
  //// Restituisce l'array del conf con ["nome_campo"] -> conf
  function getIndexedConf()
  { 
    $indexed = array();
    foreach ($this->fields as $key => $value) {

      $indexed[$value->nome_campo] = $value;
    }
    return $indexed;
  }

    /**
   * Ritorna il numero dei filtri per la tabella
   * @param  
   * @return int
   */

  function getManyFilter() 
  {
    $i = 0;
    foreach ($this->fields as  $f) {
          if($f->filter && $f->head_table) $i++;
    }
    return $i;
  }

  /**
   * Ritorna l'array delle validation per il controller   *
   * Se viene passato un id come parametro, 
   *  lo aggiunge alla rule unique per escluderlo dalla validazione
   *
   * @param  int  $no_unique_id
   * @return array
   */

//required|unique:channels,nome

 // function val_array($no_unique_id = null) {
  function val_array($no_unique_id = null,$input_array = null) {
    $arrval = array();
    $input_array = ($input_array) ? $input_array : Input::all();
    /// OTTENGO TIPO AZIONE ( se ce il no_unique_id vuol dire che sono in modifica)
    $act = (isset($no_unique_id) && $no_unique_id != null ) ? "U" : "C";

    foreach ($this->fields as $f) {
    
              //// devo controllare se il campo se è visibile o hidden .....chiamo l AdmPermessi->get_viewtp_field($permessi = null,$act)
              if($f->valid && AdmPermessi::get_viewtp_field($f->permessi,$act) != 'H' && AdmPermessi::get_viewtp_field($f->permessi,$act) != 'V' && array_key_exists($f->nome_campo,$input_array) )
                       
              {
                if($act == "U")
                {
                  $single_val = explode('|',$f->valid);
                  
                  foreach ($single_val as $v) {
                    if(substr($v, 0,6) == "unique") $f->valid .= ",".$no_unique_id[0];
                  }

                }
             //   Log::write('info',print_r($f));

              $arrval[$f->nome_campo] = $f->valid; 
              }
            
    }
    return $arrval;
  }





}

?> 