<?php
class Makeform2 {

     private static $scripts; 
     private static $tabs_link;
     
     private static function setOrNull($var,$id) {
      return (isset($var->{$id})) ? $var->{$id} : "";
     }

      public static function setOrNullCkPerm($dati,$nome_campo,$nome_permesso) {
      if( (isset($dati->{$nome_campo})) ){
        $permvalues = json_decode($dati->{$nome_campo},true);
        if (isset($permvalues[$nome_permesso])) return $permvalues[$nome_permesso];
        else return 0;
      }  
      else
      return 0;
     }


     
     /// restituisce il campo con la fx_view se è presente altrimenti restituisce il campo normale
     public static function view($data,$campo)
     {  
        $k = $campo->nome_campo;
        if($campo->fx_view) 
        { 
          $c =  explode(",", $campo->fx_view);
          if(count($c) === 2 ) return  ($data->$k) ?  $data->{$c[0]}->{$c[1]} : "";                        
          
          else if(count($c) === 1 && $c[0] != "")  return ($data->$k) ?  $data->{$c[0]}($k) : "" ;                                    
        }
        else return $data->k;
     }



     public static function getScripts() {
      return self::$scripts;
     }

   /**
   *  Ottiene le regole di validazione del campo passato come arg
   *  sotto forma di array es $validation = "required","min:32" etc
   *  @param  Field obj
   *  @return array
   */
     public static function getValidations($field)
     {
        $validations = array();
        foreach (explode('|',$field->valid) as $key => $value) {
          $validations[] = $value;
        }
        return $validations;
     }
        /**
   *  Esegue un check sul field se è required 
   *  
   *  @param  Field obj
   *  @return bool 
   */
     public static function is_required($field)
     {
        $validations = self::getValidations($field);
        return (in_array("required", $validations)) ? 1 : 0 ;
     }

    public static function generateForm($label,$risorsa){
          $xe = new Xeloquent();
          $form_conf =  $xe->f($label,$risorsa);
          $act =  $form_conf->actions[0];
          $form = Form::open($act->url, "GET", array("class" => "formfind form-horizontal") );
          
          foreach(Makeform2::createFormElement2( $form_conf , null , null) as $campo)
           { $form .= $campo["html"]; }
           $form .= '<div class="controls controls-row form-actions"> <button type="submit"  id="trova_btn" class="btn btn-info span6 offset6">
                     <i class="icon-search icon-white"></i> Cerca</button></div>';
           $form .= Form::close();   
           return $form;  
    }
    
    
    public static function tabExist($conf){
      foreach ($conf->fields as $campo) {
       if( $campo->tab_label) return true;
      }
      return false;
    } 
  /**
   *  Crea i campi multilanguage
   *  
   *  @param  Data , campo , act , array lingue , tipo campo ( text, text-large )
   *  @return bool 
   */
   public static function createMultilangField($data,$campo,$act,$lingue ) {

    $val_inlingua = array();
    $tablanglinks = array();
    if($act == 'U')  $val_inlingua = $data->getLanguagesValue($campo->nome_campo);
    $original_ncampo = $campo->nome_campo;
          foreach($lingue as $v => $lang){
              $id_campo = $campo->nome_campo;

              if( $lang->std == 0 ) {
                $new_campo = $lang->valore.'_'.$original_ncampo;
                $campo->nome_campo = $new_campo;
                $campo->label = $original_ncampo.'_'.$lang->etichetta;   
                $id_campo = $new_campo;          
                if($data) $data->$new_campo = (isset($val_inlingua[$lang->valore])) ? $val_inlingua[$lang->valore] : " ";
              }
              if($campo->tipo == "text") $html = Form::text($id_campo, Input::old($id_campo,self::setOrNull($data,$id_campo)) , array('class' => 'input-'.$campo->size,"placeholder" => $campo->place_h));
              if($campo->tipo == "text-large") $html = Form::textarea($id_campo, Input::old($id_campo,self::setOrNull($data,$id_campo)) , array('class' => 'input-'.$campo->size." textarea","id" => $id_campo,'rows' => '10',"placeholder" => $campo->place_h));
              
              $tablanglinks[] = array($lang->etichetta,$html,( $v == 0 ) ? true : false);
              $html = "";                 
            }
          

          return Tabbable::pills(  Navigation::links( $tablanglinks ) );                  
  }
    /**
   *  Crea i campi del form in base ai permessi 
   *  
   *  @param  Field obj
   *  @return bool 
   */
   public static function createFormElement2( $conf = null, $data = null, $parent = null , $notab = false ) 
   {
     $form_elements = array();
     $last_tab = '';
     $campo_parent = '';
     $tablinks = array();
     $tabcontent = "";
     $lingue = Property::get_allLanguage();

     
     /// in fase di edit o creazione venebdo da una risorsa padre
     if($parent) $campo_parent = "".$parent->model."_id";
   
     /// MOD GIULIO PERM 
     ////// ACT servono per get_view_field  : U -> modifica  C -> creazione
     if($data) $act = 'U';
     else $act = 'C'; 
     
     $first = 0;
     $tab_exist = self::tabExist($conf); 

     foreach ($conf->fields as $k => $campo) { 

       if(!$campo->abilitato) continue;
       if( AdmPermessi::get_viewtp_field($campo->permessi,$act) != 'H' ) 
       {

        if($tab_exist && !$notab) 
        {        

            if( $last_tab != '' &&  $last_tab != $campo->tab_label  ){          
           
              $tablinks[] = array($last_tab,$tabcontent,($first == 0 ) ? true : false );
              $last_tab = $campo->tab_label;  
              $tabcontent = ""; 
              $first++;
            }   
            if($last_tab == "") $last_tab = $campo->tab_label;  

            $html = self::addFormCampo( $form_elements,$campo,$act,$data,$conf,$parent,$campo_parent,null,$lingue);
            
            $campohtml = array_map(function($i) { 
                                                    return ($i["html"]) ? $i["html"] : "" ;              
                                                  }, $html);
             
            $tabcontent .=  implode("", $campohtml);              
           
            if ($k == sizeof($conf->fields)-1 ) $tablinks[] = array($last_tab,$tabcontent,($first == 0 ) ? true : false );
             
        }//if tab exist
        else  $form_elements = self::addFormCampo($form_elements,$campo,$act,$data,$conf,$parent,$campo_parent);
             
      }
     }
     return ($tab_exist && !$notab ) ? array(array("html" => Tabbable::tabs(  Navigation::links( $tablinks ) ))) : $form_elements;
    }


   /**
   *  Crea i form gestiti dal devtool
   *  @param  Field obj
   *  @return bool 
   */

     public static function createMassFormElement2( $conf = null, $data = null, $parent =null ) {

     $form_elements = array();
     $campo_parent = '';
     /// in fase di edit o creazione venebdo da una risorsa padre
     if($parent) $campo_parent = "".$parent->model."_id";
     
     /// MOD GIULIO PERM 
     ////// ACT servono per get_view_field  : U -> modifica  C -> creazione
     if($data) $act = 'U';  else $act = 'C'; 
     
     foreach ($conf->fields as $campo) { 

      if(!$campo->abilitato) continue;
      if(!$campo->massive) continue;

      $form_elements = self::addFormCampo($form_elements,$campo,$act,$data,$conf,$parent,$campo_parent,"massive");
      
     }
      return $form_elements;
    }



      /**
       *  Crea i campi del form
       *  @param  Field obj
       *  @return bool 
       */
      /// aggiunta formtype per gestire i vari di tipi ( es modifica massiva)
      public static function addFormCampo($form_elements,$campo,$act,$data,$conf, $parent = null,$campo_parent = null,$formtype = null,$lingue = null){

      $element = array();
      $combo_options = array();
      $id_campo = $campo->nome_campo;

      $campo->multilanguage = "";
      $campo->multirel = "";

      $label = (self::is_required($campo)) ? '<span class="label label-info">*</span> '."<strong>".$campo->label."</strong>" : $campo->label;

      /// USER EXIT form_field_create
      // controlla se esiste un metodo all'interno dell oggetto con nome post e nel caso lo chiama // restituisce un elemento form  ( html ) ;
      if( method_exists($conf->model, "form_field_create" )) $form_elements[] = call_user_func(array($conf->model, 'form_field_create'),$campo,$data,$act); 
      
      /////// Controllo i permessi del campo rispetto al gruppo dell'utente /////
      $viewtp = AdmPermessi::get_viewtp_field($campo->permessi,$act);

      if($viewtp == 'V' && $campo->in_form){
        //Permesso di visualizzazione - Text
        $element["html"]  = '<div class="control-group">';
        $element["html"] .= '<label for="'.$id_campo.'" class="control-label">'
                            .$label.
                            '</label><div class="controls">';          
         //$element["html"] .= Form::label($id_campo,self::setOrNull($data,$id_campo));
         $element["html"] .= self::view($data,$campo);
         $element["html"] .= '</div></div>';      
         $form_elements[] = $element;
         //Continua con il campo successivo
         return $form_elements;
      }     
      elseif($viewtp == 'H' || !$campo->in_form){
        if( $parent && ($id_campo == $campo_parent || ( $campo->multirel && $id_campo == 'id_obj' ))) {
          $val = $parent->id ; 
        }  
        else if($parent && $campo->multirel && $id_campo == 'tipo_obj'){
          $val = $parent->model;
        }
        else{
          $val = self::setOrNull($data,$id_campo);
        }
        
        //Hidden /// serve per i campi dipendenti dal parent es: disponibilità      
        $element["html"] = Form::hidden($id_campo, $val );    
        $form_elements[] = $element;
        //Continua con il campo successivo 
        return $form_elements;     
        }        
        
/// $viewtp == 'E' continua nella creazione in base al tipo del campo
///////////// FINE MOD
//////////////////////////////////////////////////////////////////////////      

      $element["html"] = '<div class="control-group" id="control-'.$id_campo.'">
                          <label for="'.$id_campo.'" class="control-label">'
                          .$label.
                          '</label><div class="controls">';

///// VALORIZZAZIONE DEL CAMPO FIGLIO CON IL VALORE DEL PARENT E SETTAGGIO DISABLED 
     
      if( $parent && ($id_campo == $campo_parent || ($campo->multirel && $id_campo == 'id_obj'))){
        $val = $id_campo;
        $element["html"] .= ''.method_exists($parent, "summary") ? $parent->summary() : $parent->id.'';
        $element["html"] .= Form::hidden($id_campo, $parent->id);
        
      }
      else if($parent && $campo->multirel && $id_campo == 'tipo_obj'){       
        $val = $id_campo;
        $element["html"] .= ''.$parent->model.'';
        $element["html"] .= Form::hidden($id_campo, $parent->model);    
        }
    
      else {
      
      
      $element["validation"] =  explode('|', $campo->valid );                     
      $element["upload"]= "";
          
      switch ( strtolower($campo->tipo) ) {

        case 'bool': //// FIX campo preimpostato
          $dropdown_options = array( 1 => "Si", 0 => "No"  );
          $preset = (isset($data->{$id_campo}) && $data->{$id_campo} != NULL ) ? $data->{$id_campo} : 0 ;          
          $element["html"] .=  Form::select($id_campo, $dropdown_options, $preset );
          break;

        case 'text':
          if($campo->multilanguage) $element["html"] .= self::createMultilangField($data,$campo,$act,$lingue);
          $element["html"] .=  Form::text($id_campo, Input::old($id_campo,self::setOrNull($data,$id_campo)) , array('class' => 'input-'.$campo->size,"placeholder" => $campo->place_h));
          break;

         case 'url':
          $element["html"] .=  Form::text($id_campo, Input::old($id_campo,self::setOrNull($data,$id_campo)) , array('class' => 'input-'.$campo->size,"placeholder" => $campo->place_h ));
          break;

        case 'text-large':

          if($campo->multilanguage) $element["html"] .= self::createMultilangField($data,$campo,$act,$lingue);
          $element["html"] .=  Form::textarea($id_campo, Input::old($id_campo,self::setOrNull($data,$id_campo)) , array('class' => 'input-'.$campo->size." textarea","id" => $id_campo,'rows' => '10',"placeholder" => $campo->place_h));
          break;

        case 'link':
           $element["html"] .=  Form::text($id_campo, Input::old($id_campo,self::setOrNull($data,$id_campo)) , array('class' => 'input-'.$campo->size,'placeholder'=>'http://'));
          break;  

        case 'autocomplete':

          if($campo->fx_view && $data) 
          { 
            $obj = IoC::resolve($campo->model)->find($data->id);
            $value = $obj->render_fxview($id_campo, $campo->combo_model.",".$campo->combo_where ,$obj);
          }else $value = null;
            
           $element["html"] .= '<div class="input-prepend">';
           $element["html"] .= '<span class="add-on"><i class="icon icon-search"></i></span>';              
           $element["html"] .= '<input class="typeahead" name="autoc__'.$id_campo.'"  type="text" data-provide="typeahead" autocomplete="off" placeholder="'.$campo->place_h.'" 
                                value="'.$value.'" data-url="'.URL::to("/typehead/").'" data-where="'.$campo->combo_where.'" data-select="'.$campo->combo_select.'" data-model="'.$campo->combo_model.'" />';
           $element["html"] .= ' <a class="btn btn-success ajax2 typeahead_btn totip" href="'.URL::to('admin/edit/'.$campo->combo_model).'" data-original-title="Crea nuovo" ><strong>+</strong></a>';
           $element["html"] .= '</div>';
          

           

           $element["html"] .= Form::hidden($id_campo ,  self::setOrNull($data,$id_campo), array("id"=>$id_campo) );
          break;

        case 'date':
          $element["html"] .= ' <div style="z-index: 10510" class="input-append date" id="dp-'.$id_campo.'" data-date="'.date('d/m/Y',strtotime('now')).'" data-date-format="dd/mm/yyyy">';
          $element["html"] .= '<';
          $element["html"] .= 'input type="text" ';     
          $element["html"] .= 'name="'.$id_campo.'" ';
          $element["html"] .= 'id="'.$id_campo.'" ';
          $element["html"] .= 'class="input-'.$campo->size.' date"';
          $element["html"] .=  'value='.self::setOrNull($data,$id_campo); 
          $element["html"] .= '>';
          $element["html"] .= '<span class="add-on"><i class="icon-th"></i></span></div>';
          $element["html"] .= '';
          break;

        case 'combo':         
          foreach (explode(",",$campo->combo_val) as $combo) { $combo_options[$combo] = $combo;   }
          $element["html"] .=  Form::select($id_campo, $combo_options, self::setOrNull($data,$id_campo));
          break;
           /// AGGIUNTA PER MULTISELECT
        case 'combo-source' || 'combo-source-multi':
          $script = "";
          $source = explode(",",$campo->combo_select); // nome,id  // nome+cognome, id
          $where = explode(",",$campo->combo_where);  // vuota     // resource_id,=,resource_id
          $model = IoC::resolve($campo->combo_model); // resource  // field
          
          $select_all = explode("+",$source[0]); $select_all[] = $source[1]; /// seleziono i campi da ottenere

          if(!$campo->combo_parent ) {  

              if(count($where)>2 ) $dropdown_options = $model->where($where[0],$where[1],$where[2])->get($select_all);
              else $dropdown_options = $model->get($select_all); // order_by('id', 'desc')->list($source[1],$source[0]);      
              

              $valore = (isset($data->{$id_campo})) ? $data->{$id_campo} : "0";   
          
          }else {  /// sono in un combo pilotato dal  parent
            $parent_c = explode(',',$campo->combo_parent); /// nome_campo , model

            if(!$data) {
                   // dd(Sentry::user()->in_group("VND"));
                    $dropdown_options = array(); // se sono in creazione lo popolo vuoto
                    ///// controllo se il campo parent è visibile o hidden
                    /// se visibile proseguo normalmente altrimenti forzo lo stato in modifica settando il campo parent con il valore hidden
                    /// modifica custom per vendor
                    if($parent_c[1] == "parent" && $parent_c[0] == "vendor_id" && Sentry::user()->in_group("VND") )
                    {

                      $dropdown_options = $model->where($where[0],$where[1],Vendor::getVndBylogged("id"))->get($select_all);   
                    }

            }
            else { // sono in modifica
                    if(count($where)>2) // obbligatorio per i combo parent 
                    {   
                      /// ottengo la lista dei valori possibili filtrando con il campo del padre 
                      $dropdown_options = $model->where($where[0],$where[1],$data->{$parent_c[0]})->get($select_all);   
                      // il valore di default sarà quello nel campo
                      $valore = $data->{$id_campo} ;   
                      
                    }/// CHIUDE IF COUNT WHERE

                  } /// else sono in modifca 

                  $script =  '$(\'select[name="'.$parent_c[0].'"]\').change( function() {
                                        var options = []; 
                                        $.getJSON("'.URL::base()."/admin/".$campo->combo_model.'/'.$parent_c[1].'/"+$(this).val()+"?cmb=1", function(data) {
                                           options.push(\'<option value="0">Selezionare</option>\');
                                            $.each(data, function(i) {                                               
                                                var that = this ;
                                                var campi = "'.$source[0].'"; var campi_etichetta = campi.split("+");                                     
                                                var etichetta = ""; 
                                                $.each( campi_etichetta , function(y,item) { etichetta += that[item]+ " " ;  });      
                                                options.push(\'<option value="\' + data[i].'.$source[1].' + \'">\' + etichetta + \'</option>\');
                                              });                    
                                            $(\'select[name="'.$id_campo.'"]\').html(options.join(""));
                                        });
                                      });';
                     

          
          } // chiude if else parent 
          $valore = (isset($valore)) ? $valore : "";
          /// creo l'array con chiave => valori_concatenati in base ai parametri passati come select nel conf  // list_array($getted, $campi , $chiave = "id")
          if ($dropdown_options) $dropdown_options_wkey = $model->list_array($dropdown_options,explode("+",$source[0]),$source[1]);
          else $dropdown_options_wkey = $dropdown_options;
          $dropdown_options_wkey["0"] = "Selezionare"; 
          ksort($dropdown_options_wkey);
          
          /// AGGIUNTA PER MULTISELECT
          $name_formfield = $id_campo;
          $extraclass = ( strtolower($campo->tipo) == "combo-source-multi" ) ? array("class" => "multiselect","multiple" => "multiple" , "data-role" => "multiselect") : array(0);
          if( strtolower($campo->tipo) == "combo-source-multi" )
          { 
            unset($dropdown_options_wkey["0"]); 
            $name_formfield = $name_formfield.'[]';
            $valore = explode(Config::get('custom.separator'), $valore);
          }
          //////////

          $element["html"] .=  Form::select($name_formfield, $dropdown_options_wkey, $valore , $extraclass);
          self::$scripts .= (isset($script)) ? $script : "";

          break;

        case 'number':

          $element["html"] .= Form::number($id_campo, self::setOrNull($data,$id_campo), array("class" => "input-".$campo->size) );
          break;
        
        case 'image':
          $element["html"] .= '<input type="hidden" value="" name="'.$id_campo.'">';        
          $element["html"] .= HTML::image($data->thumb, $data->titolo, array('width' => '150', "class" => "preview-media thumbnail"));
          break;

        case 'file':
          $element["html"] .= ' ';
          $element["html"] .= '<input type="hidden" value="" id="url-'.$id_campo.'" name="'.$id_campo.'[url]">';
          $element["html"] .= '<input type="hidden" value="file" id="type-'.$id_campo.'" name="'.$id_campo.'[type]">';      
          $element["html"] .= '<p id="nome-'.$id_campo.'"  class="nome-media"><a href="" target="_blank"></a></p>';
          $element["html"] .= '<p><button type="button" id="carica-'.$id_campo.'" class="btn btn-success btn-mini btn_upload">+ Aggiungi</button>';
          $element["html"] .= ' <button type="button" id="remove-'.$id_campo.'" class="btn btn-danger btn-mini remove-media" style="display:none" data-xms-campo="'.$id_campo.'" data-xms-type="file" >- Rimuovi</button>';
          $element["html"] .= '</p>';
          //$element["html"] .= '<a id="link-'.$id_campo.'" href=""  width="100" class="preview-media"/>';

          break;
        
        default:
          # code...
          break;
      } // chiude switch  

     }// chiude else campo = campo parent
      $element["html"] .= '<span class="help-inline " id="valid_'.$id_campo.'"></span>';
      
      if($formtype == "massive") 
        { 
          $element["html"] .= '<button class="btn btn-mini btn-success attiva_campo" id="attiva-'.$id_campo.'">Modifica</button>';
          $script_disable_input = '$(\'input[name='.$id_campo.'],select[name='.$id_campo.']\').prop("disabled","disabled");';
          self::$scripts .= (isset($script_disable_input)) ? $script_disable_input : "";
        }
    
      $element["html"] .= '<p class="helptxt"><small>'.$campo->help_tx.'</small></p>';
      $element["html"] .= '</div></div>';      
      $form_elements[] = $element;

      
      return $form_elements;
     }    

} // CHIUDE CLASSE