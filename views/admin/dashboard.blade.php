@layout('layouts.main')

@section('content')

    <div class="hero-unit">
        <h2>Benvenuto  {{$user["email"]}}</h2>
        <em>Ultimo accesso effettuato: {{ Dateutils::toita($user["last_login"],true) }} </em>        
        <p>
           Questa è la tua dashboard.
        </p>
       
    </div>

    <div class="row-fluid dashboard">
        <div class="span3">
            <?php  $house = new House();  ?>
            <h2 class="text-info">{{  count($house->getOwn())  }} Annuncio/i</h2>
            <p>Visualizza la lista completa dei tuoi annunci.<br />
            Gestisci le tue disponibilit&agrave;, aggiungi nuove foto o i nuovi servizi.</p>
            <p><a class="btn btn-primary " href="admin/house">Annunci &rarr;</a></p>
        </div>
        <?php  $order = new Order(); ?>
        <div class="span3">
             <h2 class="text-warning">{{  count($order->getOwn())  }} Prenotazione/i</h2>
            <p>Visualizza le pratiche relative ai tuoi annunci<br />
            Organizza i raporti con i tuoi clienti</p>
            <p><a class="btn btn-warning" href="admin/order">Prenotazioni &rarr;</a></p>
            
       </div> 
       <?php  $mesg = new Mesg();   ?>
        <div class="span3">
             <h2 class="text-success">{{  count($mesg->getOwn($mesg->where("readed","=",0)))  }} Nuovi Messaggi/o</h2>
            <p>Visualizza le nuove richieste inviate dagli utenti<br />
               Rispondi ai tuoi clienti nel pi&ugrave; breve tempo possibile</p>
            <p><a class="btn btn-success" href="admin/conversation">Richieste &rarr;</a></p>
            
       </div> 
        <div class="span3">
            <h2>Profilo &amp; agenti</h2>
            <p>Hai cambiato numero di telefono? Hai una nuova mail?<br />
            Gestisci i tuoi dati personali </p>
            <p><a class="btn btn-inverse" href="admin/vendor">Profilo &rarr;</a></p>    
       </div>
       <!--  <div class="span3">
            <h2>Contattaci</h2>
            <p>Non riesci a risolvere un problema? Hai delle perplessità?
            <br />Contatta il nostro help-desk e riceverai subito tutta l'assistenza di cui hai bisogno!</p>
            <p><a class="btn" href="#">Contattaci</a></p>
        </div> -->
    </div>
    
@endsection