
    	<div class="">
         
           <div class="modal-header">
           <h3>Elimina {{ $conf->fields[0]->nome_risorsa }}</h3>
           </div>
            
                  
           <div class="modal-body">
           <div id="msg">
          <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert ">&times;</button>
           <strpng>Attenzione! </strpng> L'operazione &egrave; <strong>irreversibile</strong>.
          </div> 
          {{ InappMsg::get_html() }}
          </div> 
            <h4>Confermi l'eliminazione ?</h4> 
            @if ( method_exists($data, "summary"))
              {{ $data->summary() }}
            @endif
           </div>
            <br />
         <div class="modal-footer">
            {{Form::open('admin/delete/'.$conf->model.'/'.$data->id, 'POST', Utils::form_attributes($conf->model) ) }}
            <button class="btn" data-dismiss="modal" ><i class=" icon-remove-sign"></i> Annulla</button>
            <button class="btn btn-danger ajaxform" id="crea" ><i class="icon-trash icon-white"></i> Elimina</button>
            {{ Form::close() }}
         </div>
       
     
     </div>
 



<script>



</script>
