
    <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="">
          
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
             
                @if(!isset($data))     
                   <h3> Crea {{ $conf->fields[0]->nome_risorsa }} </h3>  

                @else 
                     <h3> Modifica {{ $conf->fields[0]->nome_risorsa }} </h3>         
                              
                @endif
 
           </div>
            
          <div id="msg">
          {{ InappMsg::get_html() }}
          </div>         
          
           <div class="modal-body">
           {{Form::open('admin/edit/'.$conf->model, 'POST', Utils::form_attributes($conf->model) );}}
           
           @if(isset($data) && !$copy)   
            {{ Form::hidden('id[]',  $data->id ) }}                
           @endif 

           <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert ">&times;</button>
            I campi in <strong>grassetto</strong> sono obbligatori
          </div> 
           <fieldset>
            
           @foreach(Makeform2::createFormElement2( $conf, (isset($data)) ? $data : null, (isset($parent)) ? $parent : null ) as $campo)
           {{$campo["html"]}} 
           
           @endforeach
               
          </fieldset>
           {{ Form::close() }}
          </div>
           
         <div class="modal-footer">
           
            <button class="btn ajax-modal" data-dismiss="modal" ><i class="icon-remove-sign"></i> Chiudi</button>
            <!-- <input  type="submit" class="btn btn-primary ajaxform" id="crea" value="Salva" /> -->
            <button type="submit"  form="form_ajx" class="btn btn-primary ajaxform" id="crea" ><i class="icon-ok-sign icon-white"></i> Salva</button>

         </div>
         
      
     </div>

 



<script>
{{ Makeform2::getScripts(); }}

$(document).ready(function($) {

    $(".date").datepicker();

    if($('.multiselect').length)
      {
          $('.multiselect').multiselect();
      }


$('input.typeahead').focusout( function() {  

  if($(this).val() == "" )
  {
      var nomeinput = $(this).attr('name').split('__');
      $( "#"+nomeinput[1] ).val( 0 );
  }
});

$('input.typeahead').typeahead({
          source: function (query, process) {

                  var elem = (this.$element)
                  var nomeinput = $(elem).attr('name').split('__');
                  $( "#"+nomeinput[1] ).val( 0 );
                  var where = $(elem).data("where");
                  var select = $(elem).data("select");
                  var model = $(elem).data("model");
                  var url = $(elem).data("url");


                  $.post( url+model, { q: query , w: where , s:select  }, function ( data ) 
                  {
                      dati = {};
                      datiLabels = [];
                      _.each( data, function( item, ix, list ){ 
                           
                          label = ""; var that = item;
                           _.each( where.split(',') , function ( wh , i ) { if(i > 0 && that[wh] ) label += " - "; label += that[wh];  }  );                           
                          
                          dati[ label ] = item
                          datiLabels.push( label );                     
                      });

                  process( datiLabels );
                  });
          },

          highlighter: function(item){
                  var elem = (this.$element)
                  var nomeinput = $(elem).attr('name').split('__');
                  $( "#"+nomeinput[1] ).val( 0 );
                  var where = $(elem).data("where");
                  var select = $(elem).data("select");
                  var model = $(elem).data("model");


                  var detail = dati[ item ];
                  var secondline ="";
                  var that = this;
                  _.each( _.difference(select.split(',') , where.split(',') )   , function ( sl ,i ) { if(i > 0 && detail[sl] ) secondline += " - ";  secondline += detail[sl];  } , that  );
                  var itm = ''
                           + "<div class='typeahead_wrapper'>"
                           + "<div class='typeahead_labels'>"
                           + "<div class='typeahead_primary'>" + item +"</div>"
                           + "<div class='typeahead_secondary'>" + secondline +"</div>"
                           + "</div>"
                           + "</div>";
                  return itm;
          },
          
          updater: function (item) {
            var elem = (this.$element)
            var nomeinput = $(elem).attr('name').split('__');
            $( "#"+nomeinput[1] ).val( dati[ item ].id );

            return item;
          }
          
 });


});





</script>
