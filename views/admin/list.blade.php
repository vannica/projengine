@layout('layouts.main')

@section('content') 

    
    @render("admin.partials.pagehead",array("conf" => $conf ,"parent" => isset($parent) ? $parent : null ))

    <div class="row-fluid" id="list_body">     
  
      <div class="well">
        @render("admin.partials.actionrow",array("obj" => $obj , "conf" => $conf ,"parent" => isset($parent) ? $parent : null ))
      </div>
      
    	<table cellpadding="0" cellspacing="0" border="0" class="table table-striped  table-hover" >
    	 	<thead>
         <th style="width: 15px;"></th>
          @if($conf->actions)
          <th>Azioni</th>
         @endif 
         @foreach ($conf->fields as  $value) 

           @if($value->head_table) 
            <th  
              @if($value->filter)  
                class="filtered" 
              @endif
              >
              {{$value->label}}
            </th>
           @endif
         @endforeach
        
    	 	</thead>
       <tbody>

     
      </tbody>
      </table>
   

    </div>

    <div id="ajax-modal" class="modal hide fade " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      
      <div class="modal-body">
        <p>Caricamento...</p>
      </div>
      
    </div>


      <div id="ajax-modal2" class="modal hide fade " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-body">
        <p>Caricamento...</p>
      </div>
      <div class="modal-footer">
         <button class="btn ajax-modal" data-dismiss="modal" ><i class="icon-remove-sign"></i> Chiudi</button>
      </div>
    </div>
  
    
@endsection

@section('scripts')
   @yield('scripts-childs')
<script>
  $(document).ready(function() {

    $.fn.editable.defaults.mode = 'popup';
    active_table();

    active_ajax_get()

    active_modal();
    active_modal("#ajax-modal2",".ajax2");
    upload();


     

  });
</script>
@endsection