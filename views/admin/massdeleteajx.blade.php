
    <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="">
          {{Form::open('admin/delete/'.$conf->model, 'POST', Utils::form_attributes($conf->model) );}}
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h3>
                Elimina selezionati
              </h3>
               
           </div>
            
          <div id="msg">
          {{ InappMsg::get_html() }}
          </div>         
         
           <div class="modal-body">
           <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert ">&times;</button>
           <strpng>Attenzione! </strpng> L'operazione &egrave; <strong>irreversibile</strong>.
          </div> 
           <fieldset>
            <div id="massive_ids">
              </div>                
          </fieldset>
          
          <h5> Hai selezionato <span id="selected_num">0</span> elementi</h5>
          Confermi l&apos;eliminazione? 

          </div>
           {{ Form::close() }} 
         <div class="modal-footer">
           
            <button class="btn ajax-modal" data-dismiss="modal" ><i class="icon-remove-sign"></i> Chiudi</button>
            <button class="btn btn-danger ajaxform" id="crea" ><i class="icon-ok-sign icon-white"></i> Elimina</button>
            
         </div>
      
     </div>

 



<script>
{{ Makeform2::getScripts(); }}

$(document).ready(function($) {

    get_selectedrows();

    console.log($('#massive_ids input').length);
    $('#selected_num').text($('#massive_ids input').length);
 
});


</script>
