
      <div class="">
          {{Form::open('admin/edit/'.$conf->model, 'POST', Utils::form_attributes($conf->model) );}}
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            
                @if(!isset($data))     
                   <h3> Modifica {{$conf->name_page}} selezionati</h3>  
                   <div id="massive_ids">
                    </div>   

                @else 
                     <h3> Modifica {{$conf->name_page}} selezionati </h3>         
                                 
                @endif
 
           </div>
            
          <div id="msg">
          {{ InappMsg::get_html() }}
          </div>         
         
           <div class="modal-body">
           <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert ">&times;</button>
            I campi in <strong>grassetto</strong> sono obbligatori
          </div> 
           <fieldset>
            
           @foreach(Makeform2::createMassFormElement2( $conf, (isset($data)) ? $data : null, (isset($parent)) ? $parent : null ) as $campo)
           {{$campo["html"]}} 
           
           @endforeach
               
          </fieldset>
          </div>
           {{ Form::close() }} 
         <div class="modal-footer">
           
            <button class="btn ajax-modal" data-dismiss="modal" ><i class="icon-remove-sign"></i> Chiudi</button>
            <button class="btn btn-primary ajaxform" id="crea" ><i class="icon-ok-sign icon-white"></i> Salva</button>
            
         </div>
      
     </div>

 



<script>
{{ Makeform2::getScripts(); }}

$(document).ready(function($) {
    $('.textarea').each(function(i, elem) {
      $(elem).wysihtml5({'stylesheets': false});
    });
    $(".date").datepicker();

    get_selectedrows();
 
    // attiva campo
    $('.attiva_campo').click(function(event){
      event.preventDefault();
      var rel_id = $(this).prop("id").substring(7);
      var campo = $('input[name='+rel_id+'],select[name='+rel_id+']');
      if(campo.prop("disabled")) { 
                                    campo.prop("disabled",false);
                                    $(this).html("Ignora").removeClass("btn-success").addClass("btn-danger");
      }else {
                    campo.prop("disabled",true);
                    $(this).html("Modifica").removeClass("btn-danger").addClass("btn-success");

      }

    })

});


</script>
