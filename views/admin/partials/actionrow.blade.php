        
     @if(Sentry::user()->has_access($conf->model.'_create')) 
        
        @if(!$obj->getWUpload())
        <a href="{{ (isset($parent)) ? URL::to_action('admin/edit/'.$conf->model.'/0/'.$parent->model.'/'.$parent->id) :  URL::to_action('admin/edit/'.$conf->model) }}" 
             class="btn btn-success btn-small right ajax"><i class="icon-plus-sign icon-white"></i> <strong>Nuovo</strong>
         </a>
        @else 
        <button type="button" class="btn btn-small btn-success" data-toggle="collapse" data-target="#upl" > <i class=" icon-plus-sign icon-white"></i>
          Aggiungi
        </button>

        @endif 

      @endif
         <button type="button" class="btn btn-small btn-info" data-toggle="collapse" data-target="#filtri" ><i class=" icon-filter icon-white"></i> Filtra</button>
         <button type="button" class="btn btn-small btn-warning" id="reload"><i class="icon-repeat icon-white"></i> Aggiorna</button>
   
         <!--MASSIVE -->
         @if ($conf->getMassiveIndicator())
            
            @if(Sentry::user()->has_access($conf->model.'_update')) 
            <a href="{{ (isset($parent)) ? URL::to_action('admin/massedit/'.$conf->model.'/0/'.$parent->model.'/'.$parent->id) :  URL::to_action('admin/massedit/'.$conf->model) }}" 
             class="btn btn-small btn-inverse ajax massedit"><i class="icon-pencil icon-white"></i> Modifica Selezionati
            </a>
            @endif
            
            @if(Sentry::user()->has_access($conf->model.'_delete')) 
            <a href="{{ (isset($parent)) ? URL::to_action('admin/massdelete/'.$conf->model.'/0/'.$parent->model.'/'.$parent->id) :  URL::to_action('admin/massdelete/'.$conf->model) }}" 
             class="btn btn-small btn-danger ajax massedit"><i class="icon-remove icon-white"></i> Elimina Selezionati
            </a>
            @endif
        
         @endif
  

         <!-- INFOEXTRA -->
          <p class="pull-right">  
            <span class="label label-info">
             Elementi presenti 
            </span>
          </p> 
          <p class="pull-right" style="margin-right:10px">  
             <i class="icon-time"></i> <span id="upd_time" class=" small "></span>
          </p>
                
       <!-- UPLOAD -->
         <div id="upl" class="collapse out">
          <hr />
          <h6>
            Scegliete le immagini da caricare e cliccate su <strong>CARICA</strong>
            <br />
            <small>
              Per selezionare più immagini premere CTRL ( Win ) o CMD ( Mac ) sulla tastiera e selezionare i files
            </small>
          </h6>
          <?php $form_action = (isset($parent)) ? 'admin/'.$conf->model.'/'.$parent->model.'/'.$parent->id : 'admin/'.$conf->model.'/'; ?>
          
          {{  Form::open_for_files( $form_action, 'POST' , array("id" => "upform","class" => "form-inline")) }}
          <input class="input-file" name="file[]" id="attachments" type="file" multiple>

          <button type="button" class="btn btn-primary" id="upload">
            <i class="icon-plus-sign icon-white"></i>
            Carica
          </button>
          {{  Form::close() }}
          <br />
          <div class="progress" style="display:none" >
            <div class="bar"></div>
            <div class="percent">0%</div>
          </div>
          <div class="elaborazione text-info text-center" style="display:none" >Elaborazione immagini in corso ....</div>
        </div>

       <!-- FILTRI -->
      <form class="form-inline" >  
        <div id="filtri" class="collapse out">
        <hr />
        <span class="filter"></span> 
        <span class="filter"></span> 
         @foreach ($conf->fields as $f) 
           @if($f->head_table ) 
                @if($f->filter) <label>{{$f->label}} : </label> @endif 
                 <span class="filter"></span> 
           @endif
         @endforeach
       </div>
      </form>