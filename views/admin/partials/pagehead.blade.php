<div class="row-fluid" id="list_head">
  
  <div>
    <div class="span3">
    @if (isset($parent))  
      <a class="btn btn-mini" href="{{ URL::to( 'admin/'.$parent->model ) }}"><i class="icon-circle-arrow-left">  </i> Indietro   </a>
    @endif  
    <h2>
    {{ ($conf) ? $conf->fields[0]->nome_risorsa : $title }}
    </h2>
    
    </div>

    <div class="span8 text-info">
   
    <h3>
    {{ ( isset($parent) &&  method_exists($parent->obj, "summary") ) ? $parent->obj->summary() : ""  }}
    </h3>
    </div>
  </div>
  
</div>
<div class="clearfix">
    <span id="msg">   
      {{ InappMsg::get_html() }}
    </span>   
 </div>
