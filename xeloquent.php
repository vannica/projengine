<?php     
   /// prova modifica locale projengine
class Xeloquent extends Eloquent 
{  
   
   public static $timestamps = true;   
   public static $accessible = array();
   public static $_onlywithparent = false;
   public static $_withupload = false;

   
   public $multirel = false;
   /**
   * Serve per le query dinamiche sulle proprietà degli oggetti ( basate sull id loggato )
   * Va specificato nei model in cui è richiesto un filtro sulla proprietà degli oggetti
   * Casi:
   * Relazione con il model stesso ( es: vendor ) e campo id  - array( "tabella-model" ) 
   * Relazione con il model stesso ma con campo differente da id ( es: house ) - array( "tabella-model","campo-filtro" ) 
   * Relazione con altri model in cascata ( es: services )  - array("tabella-model1-relazionato", "tabella-model2-relazionato")
   * Relazione con altri model in cascata con relazione molti -> uno ( es: mesg )  - array(0,tabella-model1-relazionato", "tabella-model2-relazionato")
   * Nel caso di model multirel - array("nome-model-rif",tabella-model1-relazionato", "tabella-model2-relazionato" )
   * Se non viene specificato sarà ereditato questo di xeloquent che bypasserà i controlli
   */
   public $toowned = array();

   public $with = array();
   public $cmb_src_dp = array();
   

   public function __construct($attributes = array(), $exists = false)
   {
      parent::__construct($attributes, $exists);     
   }

   ///// GET STATIC PARAM
   public function getOnlyWParent() { return static::$_onlywithparent; }
   public function getWUpload() { return static::$_withupload; }

   /**
   * Ottiene gli id degli oggetti di proprietà dell'user loggato
   * in base alle relazioni definite nei model in $toowned 
   * se non viene definito viene ereditato da Xeloquent
   *
   * @param  user_id del loggato
   * @return array con id consentiti  
   */
  
   public function getOwnedIds($user_id)
   {
    $ids = array();
    $table = $this->table();
    $query = DB::table($table);
    $prev =  $table;        

    $relations = $this->toowned;


    if( !empty($relations) && is_array($relations[0])) $relations = $this->toowned[0];

     
    if(empty($relations)) return false; // se è vuoto ( quindi non specificato nel model)
   
    else if(trim($relations[0]) != $table) // se la relazione non è con se stesso
    { 
        // if($this->multirel)  da terminare
        // {
        //   $query =  $query->where( $prev.'.vendor_id','=',$user_id)->get(array($table.'.id')) ;
        // }

        ///// creo le join seguendo i campi del toowned
        foreach ($relations as $i => $value) {
            
            /// per relazioni molti a uno 
            if($i == 0 && !$value ) continue;

            if(!$i && count($relations) > 1 ) $query  = $query->join( $value , $prev.'.id', '=' , $value.'.'.Str::singular($prev).'_id');
            
            else $query  = $query->join( $value , $value.'.id', '=' , $prev.'.'.Str::singular($value).'_id');
            
            $prev = $value;
        }    
        
        /// query finale sull'oggetto e sul campo che ne stabilisce la proprieta ( in questo caso house - vendor_id)
        $idsobj = ( Sentry::user()->in_group("VND") ) 
                      ? $query->where( $value.'.vendor_id','=',$user_id)->get(array($table.'.id')) 
                      : $query->where( $value.'.agent_id','=',$user_id)->get(array($table.'.id')) ;
    }
    //// se la relazione è con se stesso leggo il second parametro 
    //// per avere il campo con cui filtrare ( se è vuoto il campo sarà id )
    else
    { 
      $field = (isset($relations[1])) ? $relations[1] : "id";
      
      if(is_array($field)) {    $field = $field[AdmPermessi::getLoggedGrpName()]; };

      $idsobj = $query->where( $field , '=' ,$user_id)->get(array($table.'.id')) ;
    } 
    
    foreach ($idsobj as $key => $value) { $ids[] = $value->id; }

    /// il secondo array definito in toowned lo uso per filtrare i dati querando sulla stessa tabella senza relazioni
    if(isset($this->toowned[1]) && is_array($this->toowned[1]) && is_array($this->toowned[0]))
    {
      $whcond = $this->toowned[1];
      if($whcond[2] == "__usermail") $whcond[2] = Sentry::user()->email;
      if($whcond[2] == "__vendorid") $whcond[2] = array_get(Vendor::where("user_id","=",Sentry::user()->id)->lists("id"),0) ;
      $ids2 = DB::table($table)->where($whcond[0],$whcond[1],$whcond[2])->lists("id");

      $ids = array_merge($ids2,$ids);

    }

    return $ids;

   }

  /**  
  * Nuova get per ottenere solo i dati di proprietà
  *
  * @param  un oggetto query opzionale per ordinamenti o altre where ( es: order_by("id","DESC") )
  * @return get()s
  */
  public function getOwn($query_obj = null , $columns = array('*') )
  {
    
    $logged = Sentry::user();
    $vnd = $this->vndid_logged($logged);
    $result = array();

    if($logged->in_group("VND") || $logged->in_group("AGT") ) $result =  $this->getOwnedIds($vnd->id);       
    else $result = false;

    if($result == false && !is_array($result) ) return ( $query_obj ) ? $query_obj->get($columns) : parent::get($columns) ; /// non è richiesta proprieta ( torna tutto )

    if(empty($result)) return array(); // nessuno oggetto di proprieta 

    else return  ($query_obj) ? $query_obj->where_in("id", $result)->get($columns) :  parent::where_in("id", $result)->get($columns); /// oggetti di proprieta
  }

   /**
  * Controllo se l'id passato come parametro è di proprietà
  * Restituisce 1 in caso affermativo altrimenti 0
  * @param  $id_obj
  * @return true o redirect all'admin
  */

  public function isOwn($id)
  { 
    if(empty($this->toowned)) return 1;    

    if($id)
    { 
      $logged = Sentry::user();
      $vnd = $this->vndid_logged($logged);
      
      if( $logged->in_group("VND") )
      {
          $owned = $this->getOwnedIds($vnd->id);

          $res = 1;          
          if( $owned == false && !is_array($owned)   )  $res = 1;
          if( !in_array( $id, $owned ) ) $res = 0;     
          return $res;
      }


     return 1;
    }

  }

   /**
   * Formatta correttamente la data in italiano in lettura di created_at e updated_at
   * 
   * @param  array campo => valore da controllare
   * @return oggetto del record se presente altrimenti 0
   */

    public function get_created_at() 
     {         
       return Dateutils::toita($this->get_attribute('created_at'),true) ;
     }

     public function set_created_at($data) 
     {         
       return  $this->set_attribute('created_at',Dateutils::tomysql($data));
     }

     public function get_updated_at() 
     {     
       return Dateutils::toita($this->get_attribute('updated_at'),true);
     }

     public function set_updated_at($data) 
     {         
       return  $this->set_attribute('updated_at',Dateutils::tomysql($data));
     }

    /**
    * Utility conversione campi bool 
    *
    */
    public function boolabel($field , $html = true, $on = "Si", $off = "No")
    {
      if($html) $val = ($this->{$field}) ? '<span class="label label-success"><i class="icon-ok icon-white"></i> '.$on.'</span>' : $off;
      else $val = ($this->{$field}) ? $on : $off;
      return $val;
    }

    public function imagePreview($field,$width = '150' , $height = '150')
    {
      return HTML::image($this->{$field}, $this->get_key() , array('width' => $width ,'height' => $height));
    } 

    public function imageLink($field , $target = "_blank")
    {
      return HTML::link($this->{$field}, "Apri immagine originale", array("target" => $target ));      
    }
    

   /**
   * Controlla se esiste un record a db passando un array con i campi da controllare 
   * ( es: house_id => 46, customer => 4, etc)
   * 
   * @param  array campo => valore da controllare
   * @return oggetto del record se presente altrimenti 0
   */
   public static function exist($campival = array())
   {  
      
      $called = get_called_class();
      $query = new $called();
      foreach ($campival as $key => $value) {
        $query = $query->where($key,"=",$value);
      }

    return $query->first();
      
   }

   
 /**
   * Crea l'array dei campi accessibili per il mass assignment
   * partendo dai field 
   * @param  fields
   * @return 
   */
    public function make_acc_array($fields) {      
      $accessible = array();
      foreach ($fields as  $f) {
         self::$accessible[] = $f->nome_campo;
      }
    }


   /**
   * Crea ( se non istanziata ) e restituisce la conf
   * 
   * @param  
   * @return obj Confmodel
   */
   public function c($label = null ,$model = null ,$id = null) {  

      //// aggiungere errore se il file non è presente
      $model = strtolower(get_class($this));
      $contents = File::get(path('app')."models/conf/".$model.".json");   
      $obj = json_decode($contents);

      $conf = new Confmodel($label,$model,$id ,$obj->fields,$obj->actions );
     
      $this->make_acc_array($obj->fields);
      
      return $conf;
   }



    /**
   * Crea ( se non istanziata ) e restituisce la conf dei form pilotati dal devtool
   * @param  
   * @return obj Confmodel
   */


   public function f($label,$risorsa,$id = null) {  

      //// aggiungere errore se il file non è presente
      $contents = File::get(path('app')."models/form_conf/".$risorsa.".json");   
      $obj = json_decode($contents);
      $conf = new Confmodel($label,$risorsa,$id ,$obj->fields,$obj->actions );     
      $this->make_acc_array($obj->fields);
      return $conf;
   }
   

    /**
   * Ottiene i dati del model elencati nell'array campi 
   *  e restituisce un array con [chiave] => concatenazione_campi
   *   ( usata per popolare i combo nei form ) 
   * @param  valori ottenuti dalla get , campi da concatenare (array), chiave
   * @return array assoc
   */
    public function list_array($getted, $campi , $chiave = "id")
    {
           
      $result = array_map(function($row) use ($campi) {
                                          $val =  array_map(function($campo) use($row) { return $row->{$campo}; } , $campi) ;                                         
                                          return  implode(" - ", $val );
                                             } , $getted);
      $keys = array_map(function($row) use ($chiave) { return $row->{$chiave};  }, $getted );
      return array_combine($keys, $result);
    }

 
   /**
   * Ottiene i with per l'EL 
   *  @param  null
   *  @return array
   */
   public function getWith()
   {
      return $this->with();
   }
   
   /**
   *  Ottiene i dati da una query passata come param e restituisce
   *  un array  
   *  @param  Query object 
   *  @return array
   */

   public function getJson($query_obj,$data)  
   {     
         $array = array();
         $parent = isset($data["parent"]) ? $data["parent"] : "";

         if($parent)  $query_obj = $query_obj->where($parent->model.'_id', '=', $parent->id); 
         foreach ($query_obj->get() as $v) {  $array[] = $v->to_array(); }

         return $array;
   }

   /**
   *  Ottiene i dati dell utente loggato
   *  un array  
   *  @param  
   *  @return array id =>  e group =>
   */

   public function getLoggedData()
   {     
        $user = array();
        $logged = Sentry::user();
        $groups = $logged->groups();
        
        $user["id"] = $logged->id;
        $user["group"] = $groups;

        return $user;
   }

   /**
   * Restituisce l'oggetto vendor legato all'user loggato
   * 
   * @param  obj Sentry user
   * @return obj Vendor dell'utente loggato
   */
     
   public function vndid_logged($logged)
   {
      return Vendor::where("user_id","=",$logged->get("id"))->first();
   }


  /// ritorna true se l'oggetto ha gia il with nel query object
   public function has_with()
   {      
      return (count($this->includes)) ? 1 : 0 ;
   }



///// MULTILANGUAGE
  public function get_langfields($label = '',$id = null) {  
    
      $model = strtolower(get_class($this));
      //// aggiungere errore se il file non è presente
      $contents = File::get(path('app')."models/conf/".$model.".json");   
      $obj = json_decode($contents);

      $conf = new Confmodel($label,$model,$id ,$obj->fields,$obj->actions );
       
      $this->make_acc_array($obj->fields);
     return $conf->get_langFields();
  } 

 /* Ottiene i dati in lingua del model */
   // ritorna array con chiave = codice lingua e valore = traduzione
   
   public function getLanguagesValue($campo = null) {
      $model = strtolower(get_class($this));
      $lang = Langdescription::where_model($model)->where_model_id($this->id);
      if($campo) $lang = $lang->where_campo($campo);
      
      return $lang->lists("valore","lingua");

   }


////////////////





////////////// FX 
   /**
   *  Ottiene i dati da una query passata come param e restituisce
   *  un Json formattato per datattabels in base al config ( quindi colonne visibili e ordinamento )
   *  @param  Query object , data ( ci sono i parametri passati dalla admin.list)
   *  @return array
   */
   public function serve($query_obj,$data )
   {     
         //Profiler::tick('init_serve');
         ////// check se il query object ha il with altrimenti lo aggiungo prima della where parent
         $query_obj = (!$this->has_with($query_obj)) ? $query_obj->with($query_obj->with) : $query_obj;
   
         $conf = $data["conf"];
         $parent = isset($data["parent"]) ? $data["parent"] : "";
          
         if($parent){
                      if($this->multirel)
                         $query_obj = $query_obj->where('id_obj', '=', $parent->id)->where('tipo_obj','=',strtolower($parent->model)); 
                      
                      else if(in_array($parent->model,$this->with)) // aggiunto if per evitare la query sul parent per i model che non hanno dipendenza
                        $query_obj = $query_obj->where($parent->model.'_id', '=', $parent->id);  
          }

         $clm = $conf->getVisibleColumns();

         // $table_field = array_keys($conf->getDbColumns()); 
         Profiler::tick('pre_get');
         
         if( get_class($query_obj) != "Laravel\Database\Eloquent\Query" && $query_obj::$timestamps) $query_obj->order_by("updated_at","DESC");
         
         //// OTTENGO I DATI DI PROPRIETA'
         $dat = $this->getOwn($query_obj); 


         ///User EXIT xq_serve_data
         if( method_exists($this , "xq_serve_data" )) $dat = $this->xq_serve_data($dat);
                  
         Profiler::tick('pre_foreach');
         
         foreach ($dat as $v) {  
          Profiler::tick('foreach obj');
            $row = array();
            
            /// CREO COLONNA SELEZIONA
            $row[] = Form::checkbox("sel_ids[]", $v->id , false, array("class"=>"selection_row") );
            
            //// Creo le action in riga
            $act_row = $this->crea_action_row($conf,$v,$parent);
           
            if($conf->actions)  $row[] = ( $act_row )  ? $act_row : " ";

            foreach ($clm as $k => $c) {
                 
                  $value = $this->render_fxview($k,$c,$v);
                
                  //// INLINE VALUE 
                  $indexed = $conf->getIndexedConf();
                  if( AdmPermessi::checkPerm("U" , $indexed[$k]->permessi ) &&  $value_inline = $this->crea_inlined_field($conf,$k,$value,$v->get_key()))  $row[] = $value_inline;
                  else $row[] = $value ;    
                  
             } 
                
            
            $dati[] = $row; 
         }
         $result["aaData"] = (isset($dati)) ? $dati : "" ;
         return $result;
   }

    /**
   *  Applica la fix_view definita nel config 
   *  @param  //$k = nome campo , $c array fx_view ( o direttamente la stringa dal conf) , $v oggetto
   *  @return string
   */ 

   public function render_fxview($k, $c ,$v)  
   {
      
      if(!is_array($c)) $c = explode(",", $c);
      
      if(count($c) >= 3 ) { $value = ($v->$k) ?  $v->{$c[0]}->{$c[1]}." ".$v->{$c[0]}->{$c[2]} : ""; }  
      elseif(count($c) === 2 ) { $value = ($v->$k) ?  $v->{$c[0]}->{$c[1]} : ""; }   /// Edge loader                    
      elseif(count($c) === 1 && $c[0] != "") { $value =   $v->{$c[0]}($k) ;   }  // FX_VIEW - passo il nome del campo alla fx_view                
      else { $value = ($v->$k) ?  $v->$k : "";    }
      return $value;
   }

   /**
   *  Crea le action per le row della dg
   *  @param  obj conf , $dato
   *  @return string
   */ 

   public function crea_action_row($conf,$v = null,$parent = null) {
        $actrows = array();
         if($v) {
            
            foreach($conf->actions as $k => $act) 
            {
              $str = "";
               if($act->for_row && $act->visible && AdmPermessi::check_perm_action($act->permessi) ) {
                  
                   //// GESTIONE PERMESSI SU CRUDV
                   if( ($act->tipo == "edit" && !Sentry::user()->has_access($conf->model.'_update'))
                      || ($act->tipo == "delete" && !Sentry::user()->has_access($conf->model.'_delete')) ) 
                    {
                      $str = " ";
                      continue;
                    }                    

                   $par = ($parent) ? $parent->model."/".$parent->id : "";   

                   if($act->fx_condition) $act = $this->{$act->fx_condition}($act,$v); 

                   // imposto la classe ajax per le action del tipo edit e delete               
                   $ajax = ($act->tipo == "edit" || $act->tipo == "delete" || $act->target == "modal" ) ? "ajax" : ""; 
                   
                   /// per le chiamate dirette in get per azioni a db
                   $ajax = ($act->tipo == "ajax") ? "ajax-get" : $ajax;  
                  
                   $target = ($act->target == "link") ? "_blank" : "_self";
                   /// aggiungo parametri alla url  se è presente parent
                   if($parent)   $param = array('id' => $v->id,'parent' => $parent->model,"id_parent" => $parent->id );
                   else $param = array('id' => $v->get_key());     

                   ///// modifica per gestione parametri query string 
                   $querystr = ""; $act_new = $act->url;
                   $acturl_extended = explode("?",$act->url);
                   if($acturl_extended) {               
                   $act_new = $acturl_extended[0];
                   if(count($acturl_extended) == 2) $querystr = "?".$acturl_extended[1];
                   }

                   ///Tooltip 
                   $tooltip = ($act->label) ? 'title="'.$act->label.'"' : "";               
                   $tool_class = ($tooltip) ? "totip" : "";
                   /////

                   $lab = ($act->icona) ? "" : $act->label;

                   $str .= '<a href="'.URL::to_action('admin/'.$act_new.'/'.$conf->model, $param ).$querystr.'" target="'.$target.'"" class="btn btn-mini '.$act->btn.' '.$ajax.' '.$tool_class.'" '.$tooltip.'>'; 
                   if($act->icona) $str .= '<i class="'.$act->icona.' icon-white"></i> ';
                   $str .= $lab.'</a> '; 


               }
              $actrows[] = $str;
            }
            return implode(" ", $actrows);
         }
   }

     //// MOD INLINE EDITING 02-06-2013
   public function crea_inlined_field($conf,$campo,$valore,$id) {
       $indexed = $conf->getIndexedConf();

       if( isset($indexed[$campo]->inline_edit) && $indexed[$campo]->inline_edit)
       {  
          $field_conf = $indexed[$campo];

          if($field_conf->tipo == "text" || $field_conf->tipo == "number" )
          {
            $str = '<a href="#" data-name="'.$campo.'" data-type="text" data-pk="'.$id.'" data-url="'.URL::base().'/admin/quickedit/'.$conf->model.'" class="inlinedit editable editable-click" >'.$valore.'</a>';
            return $str;
          } 
          if($field_conf->tipo == "bool")
          {
            $str = '<a href="#" data-name="'.$campo.'" data-type="select" data-source="{1:\'Si\',0:\'No\'}" data-pk="'.$id.'" data-url="'.URL::base().'/admin/quickedit/'.$conf->model.'" class="inlinedit editable editable-click" >'.$valore.'</a>';
            return $str;
          }

          if($field_conf->tipo == "combo")
          { 
            if(empty($this->cmb_src_dp[$field_conf->nome_campo])) {

                $dataprovider = array();
                foreach (explode(",",$field_conf->combo_val) as $combo) { $dataprovider[] = "'".$combo."'".':'."'".$combo."'"; }
                $dp_str = "{".join(",",$dataprovider)."}";  
                
                $this->cmb_src_dp[$field_conf->nome_campo] = $dp_str;
                  
            }
            $str = '<a href="#" data-name="'.$campo.'" data-type="select" data-source="'.$this->cmb_src_dp[$field_conf->nome_campo].'" data-pk="'.$id.'" data-url="'.URL::base().'/admin/quickedit/'.$conf->model.'" class="inlinedit editable editable-click" >'.$valore.'</a>';
            return $str;
          }
          if($field_conf->tipo == "combo-source")
          { 
            $source_cs = explode(",",$field_conf->combo_select); // nome,id  // nome, id
            $where_cs = explode(",",$field_conf->combo_where);  // vuota     // resource_id,=,resource_id
            $model_cs = IoC::resolve($field_conf->combo_model); // resource  // field
            
            if(empty($this->cmb_src_dp[$field_conf->nome_campo])) {
                $dataprovider = array();            
                
                if(count($where_cs)>2 ) $dataprovider = $model_cs->list_array($model_cs->getOwn($model_cs->where($where_cs[0],$where_cs[1],$where_cs[2])),explode("+",$source_cs[0]),$source_cs[1]);              
                
                else $dataprovider = $model_cs->list_array($model_cs->getOwn(),explode("+",$source_cs[0]),$source_cs[1]); // order_by('id', 'desc')->list($source[1],$source[0]);
                $dp = array();
                foreach ($dataprovider as $k => $v) { $dp[] = "'".$k."'".':'."'".$v."'"; }
                $dp_str = "{".join(",",$dp)."}";  
                
                $this->cmb_src_dp[$field_conf->nome_campo] = $dp_str;
            }

             $str = '<a href="#" data-name="'.$campo.'" data-type="select" data-source="'.$this->cmb_src_dp[$field_conf->nome_campo].'" data-pk="'.$id.'" data-url="'.URL::base().'/admin/quickedit/'.$conf->model.'" class="inlinedit editable editable-click" >'.$valore.'</a>';
            return $str;
          }

       }
       else return 0 ;
   }

////// USER_EXIT
/// ADMIN CNTRL
// public function edit_open($model = null ,$id = null, $parent = null , $id_parent = null)   {   }

// public function form_post( ) {  }

// /// MAKEFORM
// public function form_field_create($campo,$data,$act) { }

// XELOQUENT - SERVE 
/// xq_serve_data($data) -> restituisce i nuovi dati da processare

} 



